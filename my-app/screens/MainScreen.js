import React, { useState } from 'react'
import { View,StyleSheet,ImageBackground, Dimensions } from 'react-native'
import TweetInput from '../components/TweetInput'
import { TweetList } from '../components/TweetList'

const MainScreen = ({navigation}) => {

  const [tweets, setTweets] = useState([])

  return (
    <View style= {styles.container}>
       <ImageBackground
        source={require('../assets/mainScreenBG.jpg')}
        resizeMode='cover'
        style={styles.imagebackground}
        >
    <TweetList
     tweets={tweets}
     navigation={navigation}
    />
    <TweetInput
    setTweets={setTweets}
    />
    </ImageBackground>
    </View>
  )
  
}

const styles = StyleSheet.create({
  container:{
    flex:1,
    width: '100%',
    height: '100%',
   
  },

  imagebackground:{
    height: '100%'


  }
  
})



export default MainScreen