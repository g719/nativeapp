import React from 'react'
import { Text,StyleSheet, View, Pressable } from 'react-native'

const SecondScreen = ({route,navigation}) => {
  const tweet = route.params.tweet
  return (
    <View style={styles.container}>
      <Text>{tweet}</Text>
      <Pressable onPress={() => navigation.goBack()}>
        <Text style={styles.goBackStyle }> Back</Text>
        
     </Pressable>
     
    </View>
  )
}

const styles = StyleSheet.create({
  container:{
    flex:1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#c1d8f0'

  }, 
   goBackStyle:{
    backgroundColor: "dodgerblue",
    marginHorizontal: 10,
    marginVertical: 10,
    paddingVertical: 10,
    paddingHorizontal: 10,
    borderRadius: 6,
    width: 60,
    height: 40,

   }

  
})
export default SecondScreen