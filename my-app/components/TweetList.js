import { useNavigation } from '@react-navigation/native'
import React from 'react'
import { FlatList, View, Text, Pressable,StyleSheet } from 'react-native'



export const TweetList = ({tweets, navigation}) => {

    const handlePress = (tweet) =>{
    navigation.navigate('SecondScreen',{
     tweet: tweet
    }
    )

    }
    const renderItem = ({item: tweet}) =>{
       
        return(
         <View style={styles.tweet}>
          <Pressable onPress={() => 
            handlePress(tweet)
          }>
          <Text>{tweet}</Text>
        
          </Pressable>
        </View>
        )
    }
  return (
    <View style={styles.listContainer}>
    <FlatList
     data={tweets}
     renderItem={renderItem}
     keyExtractor={(tweet,index) => index}
    />

    </View>
  )
}

const styles = StyleSheet.create({

    listContainer:{
     marginTop: 15

    },

    tweet:{
    borderRadius: 4,
    flex: 1,
    backgroundColor: "white",
    marginHorizontal: 20,
    marginVertical: 5,
    padding: 10,
   
   
    
   


    }

})
