
import React, { useState } from 'react'
import { TextInput, View, StyleSheet, Pressable, Text } from 'react-native';


  const TweetInput = ({setTweets}) => {

    const [inputValue, setInputValue] = useState("")

    const handlePress = () =>{
      setTweets((prev) => prev.concat(inputValue))
      setInputValue("")
    }
    
  return (
    <View style={styles.mainContainer}>
   <View style= {styles.inputcontainer}>
   <TextInput 
   style = {styles.textInput}
   value={inputValue}
   onChangeText={setInputValue}
   />
   <Pressable
    onPress={handlePress}>
    <Text style={styles.addButton}>Add</Text>
    

   </Pressable>
   </View>
   </View>
  )
}

const styles = StyleSheet.create(
  
  {
    mainContainer:{
    flex:1,
    


    },

  inputcontainer:{
    flex:1,
    position: 'absolute',
    bottom: 10,
    height: 30,
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-evenly'
    

    },
  
    textInput:{
    backgroundColor: 'white',
    width: '75%',
    marginLeft: 10,
    paddingHorizontal: 10,
    borderRadius: 6,
    height: 30
  },
  addButton:{
    backgroundColor: "dodgerblue",
    marginHorizontal: 10,
    paddingVertical: 5,
    paddingHorizontal: 5,
    borderRadius: 6,
    width: 40,
    height: 30,
    textAlign: 'center'

   }



})


export default TweetInput;
