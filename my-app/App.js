import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View } from 'react-native';
import  MainScreen from './screens/MainScreen'
import  SecondScreen from './screens/SecondScreen';


const Stack = createNativeStackNavigator()

export default function App() {



  return (
  <NavigationContainer>
  <Stack.Navigator>
    <Stack.Screen 
    name='Tweets'
    options={ { 
    headerTitleAlign:'center'}}
    component={MainScreen} 
    />
    <Stack.Screen
    name='SecondScreen'
    options={{headerShown: false}}
    component={SecondScreen}
    />
     </Stack.Navigator>



  </NavigationContainer>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },
});
